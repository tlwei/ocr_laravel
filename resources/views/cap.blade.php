<!DOCTYPE HTML>
<html>
<head>  
		<link href="{{ asset('/app.css') }}" rel="stylesheet">
<script>
window.onload = function () {
 
var deb = new CanvasJS.Chart("debEq", {
	animationEnabled: true,

	theme: "light1", // "light1", "light2", "dark1", "dark2"
	title:{
		text: <?php echo json_encode($debEq[0]['title'], JSON_NUMERIC_CHECK); ?>
	},
	data: [{
		type: "line", //change type to bar, line, area, pie, etc
		//indexLabel: "{y}", //Shows y value on all Data Points
		name:<?php echo json_encode($debEq[0]['type'], JSON_NUMERIC_CHECK); ?>,
		indexLabel: "{y}",
		showInLegend: true,
		
		dataPoints: <?php echo json_encode($debEq[1], JSON_NUMERIC_CHECK); ?>
	}]
});

var retain = new CanvasJS.Chart("retainValue", {
	animationEnabled: true,

	theme: "light1", // "light1", "light2", "dark1", "dark2"
	title:{
		text: <?php echo json_encode($retainValue[0]['title'], JSON_NUMERIC_CHECK); ?>
	},
	data: [{
		type: "line", //change type to bar, line, area, pie, etc
		//indexLabel: "{y}", //Shows y value on all Data Points
		name:<?php echo json_encode($retainValue[0]['type'], JSON_NUMERIC_CHECK); ?>,
		indexLabel: "{y}",
		showInLegend: true,
		
		dataPoints: <?php echo json_encode($retainValue[1], JSON_NUMERIC_CHECK); ?>
	}]
});

deb.render();
retain.render();
}	

</script>
</head>
<body>
	<div>
		<input class="no-print" type="button" value="Back to menu" onclick="window.history.back()" />
		<button onclick="window.print();" class="no-print"> Print to PDF </button>
	<h4>
		@php echo $title @endphp <br>
		Cap Structure graphs
	</h4>
	<div id="debEq" class="chart"></div>
	<div id="retainValue" class="chart"></div>
</div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>  