<!DOCTYPE HTML>
<html>
<head>  
    <link href="{{ asset('/app.css') }}" rel="stylesheet">
    <script>
        window.onload = function () {
         
         
          var tur = new CanvasJS.Chart("turValue", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($turValue['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($turValue['ar']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($turValue['ar'][0], JSON_NUMERIC_CHECK); ?>
            },{
              markerType: "square",
              type: "line",
              name:<?php echo json_encode($turValue['ap']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($turValue['ap'][0], JSON_NUMERIC_CHECK); ?>
            },{
              markerType: "triangle",
              type: "line",
              name:<?php echo json_encode($turValue['inv']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($turValue['inv'][0], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var outstading = new CanvasJS.Chart("outstandingValue", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title:{
              text: <?php echo json_encode($outstandingValue['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "column",
              name:<?php echo json_encode($outstandingValue['ar']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($outstandingValue['ar'][0], JSON_NUMERIC_CHECK); ?>
            },{
              type: "column",
              name:<?php echo json_encode($outstandingValue['inv']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($outstandingValue['inv'][0], JSON_NUMERIC_CHECK); ?>
            },{
              type: "column",
              name:<?php echo json_encode($outstandingValue['ap']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($outstandingValue['ap'][0], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var cycle = new CanvasJS.Chart("cycleValue", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title:{
              text: <?php echo json_encode($cycleValue['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "column",
              name:<?php echo json_encode($cycleValue['op']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($cycleValue['op'][0], JSON_NUMERIC_CHECK); ?>
            },{
              type: "column",
              name:<?php echo json_encode($cycleValue['cash']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($cycleValue['cash'][0], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var totalTur = new CanvasJS.Chart("totalTur", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($totalTur[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($totalTur[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($totalTur[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var fixTue = new CanvasJS.Chart("fixTue", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($fixTue[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($fixTue[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($fixTue[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var reEq = new CanvasJS.Chart("reEq", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($reEq[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($reEq[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($reEq[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
          
          tur.render();
          outstading.render();
          cycle.render();
          totalTur.render();
          fixTue.render();
          reEq.render();
         
        }	
        
        </script>
  </head>
  <body>
      <div>
        <input class="no-print" type="button" value="Back to menu" onclick="window.history.back()" />
        <button onclick="window.print();" class="no-print"> Print to PDF </button>
       
        <div class='break'>
            <h4>
              @php echo $title @endphp <br>
              Efficiency Graphs
            </h4>
            <div id="turValue" class='chart'></div>
            <div id="outstandingValue" class='chart'></div>
            <div id="cycleValue" class='chart break'></div>
            <div id="totalTur" class='chart'></div>
            <div id="fixTue" class='chart'></div>
            <div id="reEq" class='chart'></div>
        </div>
        
    </div>
  </body>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</html>   