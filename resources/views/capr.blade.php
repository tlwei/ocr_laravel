<!DOCTYPE HTML>
<html>
<head>  
    <link href="{{ asset('/app.css') }}" rel="stylesheet">
   
  </head>
  <body>
      <div>
        <input class="no-print" type="button" value="Back to menu" onclick="window.history.back()" />
        <button onclick="window.print();" class="no-print"> Print to PDF </button>
       
        
        <div class='break'>
            <h4>
                @php echo $title @endphp <br>
                Capital Stucture Analysis<br>
              </h4>
              <table width="100%" border="1">
                  @foreach ($csRatio as $bodyKey => $bodyItem)
                  <thead>
                    <tr style="text-align: left">
                      @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                          <th>{{$bodyValueKey}}</th>
                      @endforeach
                    </tr>
                  </thead>
                  <tbody>
                    <tr style="text-align: left">
                      @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                        @if (is_array($bodyValueItem) == false)
                          <th>{{$bodyValueItem}}</th>
                        @else 
                          <th>{{$bodyValueItem[1]}} ({{$bodyValueItem[0]}})</th>
                        @endif
                      @endforeach
                    </tr>
                  </tbody>
                  @endforeach
              </table><br>
        </div>

       
  </body>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</html>   