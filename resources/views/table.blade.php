<!DOCTYPE HTML>
<html>
<head>  
    <link href="{{ asset('/app.css') }}" rel="stylesheet">
    <script>
        window.onload = function () {
         
          var deb = new CanvasJS.Chart("debEq", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title:{
              text: <?php echo json_encode($debEq[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line", //change type to bar, line, area, pie, etc
              //indexLabel: "{y}", //Shows y value on all Data Points
              name:<?php echo json_encode($debEq[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($debEq[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
          
          var retain = new CanvasJS.Chart("retainValue", {
            
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title:{
              text: <?php echo json_encode($retainValue[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line", //change type to bar, line, area, pie, etc
              //indexLabel: "{y}", //Shows y value on all Data Points
              name:<?php echo json_encode($retainValue[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($retainValue[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
          
          var work = new CanvasJS.Chart("workCap", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title:{
              text: <?php echo json_encode($workCap[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "column", //change type to bar, line, area, pie, etc
              //indexLabel: "{y}", //Shows y value on all Data Points
              name:<?php echo json_encode($workCap[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              yValueFormatString: "$#0.##",
              showInLegend: true,
              dataPoints: <?php echo json_encode($workCap[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
          
          var ratio = new CanvasJS.Chart("curRatio", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($ratioValue['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($ratioValue['cur']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($ratioValue['cur'][0], JSON_NUMERIC_CHECK); ?>
            },{
              type: "line",
              name:<?php echo json_encode($ratioValue['test']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($ratioValue['test'][0], JSON_NUMERIC_CHECK); ?>
            }]
          });
          var tur = new CanvasJS.Chart("turValue", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($turValue['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($turValue['ar']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($turValue['ar'][0], JSON_NUMERIC_CHECK); ?>
            },{
              markerType: "square",
              type: "line",
              name:<?php echo json_encode($turValue['ap']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($turValue['ap'][0], JSON_NUMERIC_CHECK); ?>
            },{
              markerType: "triangle",
              type: "line",
              name:<?php echo json_encode($turValue['inv']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($turValue['inv'][0], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var outstading = new CanvasJS.Chart("outstandingValue", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title:{
              text: <?php echo json_encode($outstandingValue['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "column",
              name:<?php echo json_encode($outstandingValue['ar']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($outstandingValue['ar'][0], JSON_NUMERIC_CHECK); ?>
            },{
              type: "column",
              name:<?php echo json_encode($outstandingValue['inv']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($outstandingValue['inv'][0], JSON_NUMERIC_CHECK); ?>
            },{
              type: "column",
              name:<?php echo json_encode($outstandingValue['ap']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($outstandingValue['ap'][0], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var cycle = new CanvasJS.Chart("cycleValue", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title:{
              text: <?php echo json_encode($cycleValue['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "column",
              name:<?php echo json_encode($cycleValue['op']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($cycleValue['op'][0], JSON_NUMERIC_CHECK); ?>
            },{
              type: "column",
              name:<?php echo json_encode($cycleValue['cash']['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($cycleValue['cash'][0], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var totalTur = new CanvasJS.Chart("totalTur", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($totalTur[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($totalTur[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($totalTur[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var fixTue = new CanvasJS.Chart("fixTue", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($fixTue[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($fixTue[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($fixTue[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
        
          var reEq = new CanvasJS.Chart("reEq", {
            animationEnabled: true,
            
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
              text: <?php echo json_encode($reEq[0]['title'], JSON_NUMERIC_CHECK); ?>
            },
            data: [{
              type: "line",
              name:<?php echo json_encode($reEq[0]['type'], JSON_NUMERIC_CHECK); ?>,
              indexLabel: "{y}",
              showInLegend: true,
              dataPoints: <?php echo json_encode($reEq[1], JSON_NUMERIC_CHECK); ?>
            }]
          });
          
          tur.render();
          outstading.render();
          cycle.render();
          totalTur.render();
          fixTue.render();
          reEq.render();
          work.render();
          ratio.render();
          deb.render();
          retain.render();
        }	
        
        </script>
  </head>
  <body>
      <div>
        <input class="no-print" type="button" value="Back to menu" onclick="window.history.back()" />
        <button onclick="window.print();" class="no-print"> Print to PDF </button>
       
        <div class='break'>
          <h4>
            @php echo $title @endphp <br>
            Consolidated Balance Sheets<br>
            Input Worksheet
          </h4>
          
          @foreach ($bsinput as $key => $item)
            <div style="text-align: left">
              @if($key == 0)
                Assets<br>Current Assets
              @else
                Liabilities & Shareholders' Equity <br>Current liabilities
              @endif
            </div>
            <table width="100%" border="1">
              <thead>
               
                <tr style="text-align: left">
                  @foreach ($item[0] as $headKey => $headItem)
                    <th>{{$headKey}}</th>
                  @endforeach
                </tr>
              </thead>
              
              <tbody>
                @foreach ($item as $bodyKey => $bodyItem)
                  <tr style="text-align: left">
                    @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                      <th>{{$bodyValueItem}}</th>
                    @endforeach
                  </tr>
                @endforeach
              </tbody>
            </table>
          @endforeach
        </div>

        <div class='break'>
          <h4>
            @php echo $title @endphp <br>
            Consolidated Income Statement<br>
            Input Worksheet
          </h4>
          <table width="100%" border="1">
            <thead>
              <tr style="text-align: left">
                @foreach ($plinput[0] as $bodyValueKey => $bodyValueItem)
                  <th>{{$bodyValueKey}}</th>
                @endforeach
              </tr>
            </thead>
            <tbody>
              @foreach ($plinput as $bodyKey => $bodyItem)
                <tr style="text-align: left">
                  @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                    <th>{{$bodyValueItem}}</th>
                  @endforeach
                </tr>
              @endforeach
            </tbody>
          </table> 
        </div>

        <div class='break'>
            <h4>
                @php echo $title @endphp <br>
                Historical Data by Month<br>
                Input Worksheet
              </h4>
              <table width="100%" border="1">
              @foreach ($hisinput as $key => $item)
              
                @foreach ($item as $bodyKey => $bodyItem)
                  @if($bodyKey == 0)
                    
                  @else
                    <thead>
                      @if($bodyKey == 1)
                      <tr style="text-align: left">
                        <th style="background-color: white">
                          <div>{{$item[0]}}</div>
                        </th>
                        </tr>
                        <tr style="text-align: left">
                          @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                            <th>{{$bodyValueKey}}</th>
                          @endforeach
                        </tr>
                      @endif
                    </thead>
                    <tbody>
                      <tr style="text-align: left">
                        @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                          <th>{{$bodyValueItem}}</th>
                        @endforeach
                      </tr>
                    </tbody>
                  @endif
                @endforeach
              @endforeach
            </table> 
        </div>

        <div class='break'>
            <h4>
                @php echo $title @endphp <br>
                Consolidated Balance Sheet<br>
            </h4>
            
              @foreach ($bsoutput as $key => $item)
              <div style="text-align: left">
                @if($key == 0)
                  Assets<br>Current Assets
                @else
                  Liabilities & Shareholders' Equity <br>Current liabilities
                @endif
              </div>
              <table width="100%" border="1">
                  <thead>
                    
                    <tr style="text-align: left">
                      @foreach ($item[0] as $headKey => $headItem)
                        <th>{{$headKey}}</th>
                      @endforeach
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($item as $bodyKey => $bodyItem)
                      <tr style="text-align: left">
                        @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                          <th>{{$bodyValueItem}}</th>
                        @endforeach
                      </tr>
                    @endforeach
                 
                  </tbody>
                </table>
              @endforeach
        </div>

        <div class='break'>
            <h4>
                @php echo $title @endphp <br>
                Consolidated Income Statement<br>
                Input Worksheet
              </h4>
              <table width="100%" border="1">
                <thead>
                  <tr style="text-align: left">
                    @foreach ($ploutput[0] as $bodyValueKey => $bodyValueItem)
                      <th>{{$bodyValueKey}}</th>
                    @endforeach
                  </tr>
                </thead>
                <tbody>
                  @foreach ($ploutput as $bodyKey => $bodyItem)
                    <tr style="text-align: left">
                      @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                        <th>{{$bodyValueItem}}</th>
                      @endforeach
                    </tr>
                  @endforeach
                </tbody>
              </table> 
        </div>

        <div class='break'>
            <h4>
                @php echo $title @endphp <br>
                Restated Consolidated Balance Sheets<br>
                (reclassifies shareholders notes to equity)
              </h4>
              
              @foreach ($bsrestate as $key => $item)
              <div style="text-align: left">
                  @if($key == 0)
                    Assets<br>Current Assets
                  @else
                    Liabilities & Shareholders' Equity <br>Current liabilities
                  @endif
              </div>
              <table width="100%" border="1">
                  <thead>
                    
                    <tr style="text-align: left">
                      @foreach ($item[0] as $headKey => $headItem)
                        <th>{{$headKey}}</th>
                      @endforeach
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($item as $bodyKey => $bodyItem)
                      <tr style="text-align: left">
                        @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                          <th>{{$bodyValueItem}}</th>
                        @endforeach
                      </tr>
                    @endforeach
                 
                  </tbody>
                </table>
              @endforeach
        </div>

        <div class='break'>
            <h4>
                @php echo $title @endphp <br>
                Banking Ratios<br>
                These are the ratios and their desired values that bankers usually require to extend credit.
              </h4>
              <table width="100%" border="1">
                <thead>
                  <tr style="text-align: left">
                    @foreach ($bankRatio[0] as $bodyValueKey => $bodyValueItem)
                      <th>{{$bodyValueKey}}</th>
                    @endforeach
                  </tr>
                </thead>
                <tbody>
                  @foreach ($bankRatio as $bodyKey => $bodyItem)
                    <tr style="text-align: left">
                      @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                        <th>{{$bodyValueItem}}</th>
                      @endforeach
                    </tr>
                  @endforeach
                </tbody>
              </table>
        </div>

        <div class='break'>
            <h4>
                @php echo $title @endphp <br>
                Liquidity Analysis<br>
              </h4>
              <table width="100%" border="1">
                  @foreach ($lRatios as $bodyKey => $bodyItem)
                  <thead>
                    <tr style="text-align: left">
                      @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                          <th>{{$bodyValueKey}}</th>
                      @endforeach
                    </tr>
                  </thead>
                  <tbody>
                    <tr style="text-align: left">
                      @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                        @if (is_array($bodyValueItem) == false)
                          <th>{{$bodyValueItem}}</th>
                        @else 
                          <th>{{$bodyValueItem[1]}} ({{$bodyValueItem[0]}})</th>
                        @endif
                      @endforeach
                    </tr>
                  </tbody>
                  @endforeach
              </table>
        </div>

        <div class='break'>
            <h4>
                @php echo $title @endphp <br>
                Capital Stucture Analysis<br>
              </h4>
              <table width="100%" border="1">
                  @foreach ($csRatio as $bodyKey => $bodyItem)
                  <thead>
                    <tr style="text-align: left">
                      @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                          <th>{{$bodyValueKey}}</th>
                      @endforeach
                    </tr>
                  </thead>
                  <tbody>
                    <tr style="text-align: left">
                      @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                        @if (is_array($bodyValueItem) == false)
                          <th>{{$bodyValueItem}}</th>
                        @else 
                          <th>{{$bodyValueItem[1]}} ({{$bodyValueItem[0]}})</th>
                        @endif
                      @endforeach
                    </tr>
                  </tbody>
                  @endforeach
              </table><br>
        </div>

        <div class='break'>
            <h4>
              @php echo $title @endphp <br>
              Analysis of Operating Efficiency
            </h4>
            <table width="100%" border="1">
                @foreach ($effRatio as $bodyKey => $bodyItem)
     

                    <thead>
                      
                      <tr style="text-align: left">
                        @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                            <th>{{$bodyValueKey}}</th>
                        @endforeach
                      </tr>
                    </thead>
                    <tbody>
                      <tr style="text-align: left">
                        @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                          @if (is_array($bodyValueItem) == false)
                            <th>{{$bodyValueItem}}</th>
                          @else 
                            <th>{{$bodyValueItem[1]}} ({{$bodyValueItem[0]}})</th>
                          @endif
                        @endforeach
                      </tr>
                    </tbody>
                 
                  @endforeach
                </table>
        </div>

        <div class='break'>
          <h4>
            @php echo $title @endphp <br>
            Z-Score Model
          </h4>
          @foreach ($zScore as $bodyKey => $bodyItem)
            @if ($bodyKey == 0)
              <br>Definition:<br>			
              NYU Professor Edward Altman developed this model through a study of 33 bankrupt companies.  His research	<br>				
              indicated that bankruptcy can be predicted up to two years prior to the event through ratio analysis.	<br>				         
              Scoring: <br>
            @else
              <br>Note, however, that each company's Z-Score may be relative to its line of business and stage of development.<br>					
              This tool is best used by tracking Z-Scores over time.<br>			
              Calculation:<br>					
              (6.56 x working capital/total assets) + (3.26 x retained earnings/total assets) + (6.72 x EBIT/total assets) + (1.05 x stockholders' equity/total liabilities) <br> 					
              Results:<br>
            @endif
            <table width="100%" border="1">
              <thead>
                <tr style="text-align: left">
                  @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                    <th>{{$bodyValueKey}}</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                <tr style="text-align: left">
                  @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                    @if (is_array($bodyValueItem) == false)
                      <th>{{$bodyValueItem}}</th>
                    @else 
                      <th>{{$bodyValueItem[1]}} ({{$bodyValueItem[0]}})</th>
                    @endif
                  @endforeach
                </tr>
              </tbody>
            </table>
          @endforeach
        </div>

        <div class='break'>
            <h4>
              @php echo $title @endphp <br>
              Liquidity Graphs
            </h4>
            <div id="workCap" class='chart'></div>
            <div id="curRatio" class='chart'></div>
        </div>

        <div class='break'>
            <h4>
              @php echo $title @endphp <br>
              Cap Structure graphs
            </h4>
            <div id="debEq" class='chart'></div>
            <div id="retainValue" class='chart'></div>
        </div>

        <div class='break'>
            <h4>
              @php echo $title @endphp <br>
              Efficiency Graphs
            </h4>
            <div id="turValue" class='chart'></div>
            <div id="outstandingValue" class='chart break'></div>
            <div id="cycleValue" class='chart'></div>
            <div id="totalTur" class='chart break'></div>
            <div id="fixTue" class='chart'></div>
            <div id="reEq" class='chart'></div>
        </div>
        
    </div>
  </body>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</html>   