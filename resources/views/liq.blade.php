<!DOCTYPE HTML>
<html>
<head>  
		<link href="{{ asset('/app.css') }}" rel="stylesheet">
<script>
window.onload = function () {
 
var work = new CanvasJS.Chart("workCap", {
	animationEnabled: true,

	theme: "light1", // "light1", "light2", "dark1", "dark2"
	title:{
		text: <?php echo json_encode($workCap[0]['title'], JSON_NUMERIC_CHECK); ?>
	},
	data: [{
		type: "column", //change type to bar, line, area, pie, etc
		//indexLabel: "{y}", //Shows y value on all Data Points
		name:<?php echo json_encode($workCap[0]['type'], JSON_NUMERIC_CHECK); ?>,
		indexLabel: "{y}",
		yValueFormatString: "$#0.##",
		showInLegend: true,
		
		dataPoints: <?php echo json_encode($workCap[1], JSON_NUMERIC_CHECK); ?>
	}]
});

var ratio = new CanvasJS.Chart("curRatio", {
	animationEnabled: true,

	theme: "light1", // "light1", "light2", "dark1", "dark2"
	title: {
		text: <?php echo json_encode($ratioValue['title'], JSON_NUMERIC_CHECK); ?>
	},
	data: [{
		type: "line",
		name:<?php echo json_encode($ratioValue['cur']['type'], JSON_NUMERIC_CHECK); ?>,
		indexLabel: "{y}",
		showInLegend: true,
		dataPoints: <?php echo json_encode($ratioValue['cur'][0], JSON_NUMERIC_CHECK); ?>
	},{
		type: "line",
		name:<?php echo json_encode($ratioValue['test']['type'], JSON_NUMERIC_CHECK); ?>,
		indexLabel: "{y}",
		showInLegend: true,
		dataPoints: <?php echo json_encode($ratioValue['test'][0], JSON_NUMERIC_CHECK); ?>
	}]
});

work.render();
ratio.render();
}	

</script>
</head>
<body>
	<div>
		<input class="no-print" type="button" value="Back to menu" onclick="window.history.back()" />
		<button onclick="window.print();" class="no-print"> Print to PDF </button>
		<h4>
			@php echo $title @endphp <br>
			Liquidity Graphs
		</h4>
		<div id="workCap" class="chart"></div>
		<div id="curRatio" class="chart"></div>
	</div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>  