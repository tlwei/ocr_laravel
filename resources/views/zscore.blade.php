<!DOCTYPE HTML>
<html>
<head>  
    <link href="{{ asset('/app.css') }}" rel="stylesheet">
  </head>
  <body>
      <div>
        <input class="no-print" type="button" value="Back to menu" onclick="window.history.back()" />
        <button onclick="window.print();" class="no-print"> Print to PDF </button>

        <div class='break'>
          <h4>
            @php echo $title @endphp <br>
            Z-Score Model
          </h4>
          @foreach ($zScore as $bodyKey => $bodyItem)
            @if ($bodyKey == 0)
              <br>Definition:<br>			
              NYU Professor Edward Altman developed this model through a study of 33 bankrupt companies.  His research	<br>				
              indicated that bankruptcy can be predicted up to two years prior to the event through ratio analysis.	<br>				         
              Scoring: <br>
            @else
              <br>Note, however, that each company's Z-Score may be relative to its line of business and stage of development.<br>					
              This tool is best used by tracking Z-Scores over time.<br>			
              Calculation:<br>					
              (6.56 x working capital/total assets) + (3.26 x retained earnings/total assets) + (6.72 x EBIT/total assets) + (1.05 x stockholders' equity/total liabilities) <br> 					
              Results:<br>
            @endif
            <table width="100%" border="1">
              <thead>
                <tr style="text-align: left">
                  @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                    <th>{{$bodyValueKey}}</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                <tr style="text-align: left">
                  @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                    @if (is_array($bodyValueItem) == false)
                      <th>{{$bodyValueItem}}</th>
                    @else 
                      <th>{{$bodyValueItem[1]}} ({{$bodyValueItem[0]}})</th>
                    @endif
                  @endforeach
                </tr>
              </tbody>
            </table>
          @endforeach
        </div>

        
  </body>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</html>   