<!DOCTYPE HTML>
<html>
<head>  
    <link href="{{ asset('/app.css') }}" rel="stylesheet">
  </head>
  <body>
      <div>
        <input class="no-print" type="button" value="Back to menu" onclick="window.history.back()" />
        <button onclick="window.print();" class="no-print"> Print to PDF </button>
       
        <div class='break'>
          <h4>
            @php echo $title @endphp <br>
            Consolidated Income Statement <br>
            Input Worksheet
          </h4>
          <table width="100%" border="1">
            <thead>
              <tr style="text-align: left">
                @foreach ($plinput[0] as $bodyValueKey => $bodyValueItem)
                  <th>{{$bodyValueKey}}</th>
                @endforeach
              </tr>
            </thead>
            <tbody>
              @foreach ($plinput as $bodyKey => $bodyItem)
                <tr style="text-align: left">
                  @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                    <th>{{$bodyValueItem}}</th>
                  @endforeach
                </tr>
              @endforeach
            </tbody>
          </table> 
        </div>

  </body>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</html>   