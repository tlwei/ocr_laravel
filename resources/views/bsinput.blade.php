<!DOCTYPE HTML>
<html>
<head>  
    <link href="{{ asset('/app.css') }}" rel="stylesheet">

  </head>
  <body>
      <div>
        <input class="no-print" type="button" value="Back to menu" onclick="window.history.back()" />
        <button onclick="window.print();" class="no-print"> Print to PDF </button>
       
        <div class='break'>
          <h4>
            @php echo $title @endphp <br>
            Consolidated Balance Sheets<br>
            Input Worksheet
          </h4>
          
          @foreach ($bsinput as $key => $item)
            <div style="text-align: left">
              @if($key == 0)
                Assets<br>Current Assets
              @else
                Liabilities & Shareholders' Equity <br>Current liabilities
              @endif
            </div>
            <table width="100%" border="1">
              <thead>
               
                <tr style="text-align: left">
                  @foreach ($item[0] as $headKey => $headItem)
                    <th>{{$headKey}}</th>
                  @endforeach
                </tr>
              </thead>
              
              <tbody>
                @foreach ($item as $bodyKey => $bodyItem)
                  <tr style="text-align: left">
                    @foreach ($bodyItem as $bodyValueKey => $bodyValueItem)
                      <th>{{$bodyValueItem}}</th>
                    @endforeach
                  </tr>
                @endforeach
              </tbody>
            </table>
          @endforeach
        </div>

      
  </body>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</html>   