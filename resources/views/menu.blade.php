<!DOCTYPE html>
<html>
<title>FR</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<body>

<!-- Navbar (sit on top) -->
<div class="header w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
      <a href="/" class="w3-bar-item w3-button"><b>FR</b> FinanceReport</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">

        <a href="/" class="w3-bar-item w3-button">About</a>
        <a href="/" class="w3-bar-item w3-button">Contact</a>
    </div>
  </div>
</div>

<!-- Header -->
<header class="w3-display-container w3-content w3-wide" style="max-width:1500px;" id="home">
  <br><br><br><br>
  <h1 align="center">The R Series: Print Manager</h1>
        <h3 align="center">Title Campany</h3>
        <div class="card-group">
            <div class="card">
                <div class="card-header">

                    <form action='fullReport' method="POST">
                        <input type='hidden' name="type" value="bsoutput"/>
                        <input type='hidden' name="report" value="{{$fullReport}}"/>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Financial Statement Pack</button>
                    </form>
                    
                </div>
                <div class="card-body">    
                    <h5 class="card-title">
          
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="bsoutput"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Balance Sheet</button>
                        </form><p>
         
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="ploutput"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">P&L Statement</button>
                        </form>

                    </h5>
                </div>
                <div class="card-footer text-muted">
        
                  <form action='fullReport' method="POST">
                      <input type='hidden' name="type" value="full"/>
                      <input type='hidden' name="report" value="{{$fullReport}}"/>
                      <button type="submit" class="btn btn-primary btn-lg btn-block">** FULL REPORT **</button>
                  </form>
                    
                </div>
            </div>

            <div class="card">
                <div class="card-header">
              
                  <form action='fullReport' method="POST">
                      <input type='hidden' name="type" value="liqr"/>
                      <input type='hidden' name="report" value="{{$fullReport}}"/>
                      <button type="submit" class="btn btn-primary btn-lg btn-block">Analysis Pack</button>
                  </form>
                  
                </div>
                <div class="card-body">
                    <h5 class="card-title">
                
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="liqr"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Liquidity Analysis</button>
                        </form><p>
                    
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="capr"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Capital Stucture Analysis</button>
                        </form><p>
            
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="effr"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Efficiency Analysis</button>
                        </form><p>
                 
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="bank"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Banking Analysis</button>
                        </form><p>
                 
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="zscore"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Z-Score Analysis</button>
                        </form>
                    </h5>
              </div>
            </div>

            <div class="card">
                <div class="card-header">
                
                  <form action='fullReport' method="POST">
                      <input type='hidden' name="type" value="liq"/>
                      <input type='hidden' name="report" value="{{$fullReport}}"/>
                      <button type="submit" class="btn btn-primary btn-lg btn-block">Graph Pack</button>
                  </form>
                </div>
                <div class="card-body">
                    <h5 class="card-title">
                  
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="liq"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Liquidity Graphs</button>
                        </form><p>
                        
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="cap"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Capital Stucture Graphs</button>
                        </form><p>
                    
                        <form action='fullReport' method="POST">
                            <input type='hidden' name="type" value="eff"/>
                            <input type='hidden' name="report" value="{{$fullReport}}"/>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Efficiency Graphs</button>
                        </form>
                    </h5>
                </div>
            </div>
          </div>

</header>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
