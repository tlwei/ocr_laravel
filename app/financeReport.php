<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class financeReport extends Model
{
    //
    protected $table = 'finance_report';
    protected $fillable = [
        'title', 'value','file_type'
    ];
}
