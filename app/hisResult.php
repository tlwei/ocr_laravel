<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hisResult extends Model
{
    //
    protected $table = 'his_result';
    protected $fillable = [
        'title', 'value','file_type','year'
    ];
}
