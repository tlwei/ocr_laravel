<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bsResult extends Model
{
    //
    protected $table = 'bs_result';
    protected $fillable = [
        'title', 'value','file_type'
    ];
}
