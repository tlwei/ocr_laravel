<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Spatie\PdfToText\Pdf;
use PDFA;
use App\financeReport;
use thiagoalessio\TesseractOCR\TesseractOCR;

class ocrController extends Controller
{
    public $fileBs;
    public $filePl;
    public $fileHis;
    public $title;
    public $pdf_path = '/usr/local/bin/pdftotext';

    public function json(Request $request){
        $json = file_get_contents('template/test_report.json');
        $old = json_decode($json);
        $data = $old;
        
        $new = [rand(1,10),rand(1,10),rand(1,10),rand(1,10)];
        $data[] = $new;

        $json_data = json_encode($data);
        file_put_contents('template/test_report.json', $json_data);
        return $json;
    }

    public function getValue(Request $request) {

        $this->fileBs = $request['fileBs'];
        $this->filePl = $request['filePl'];
        $this->fileHis = $request['fileHis'];
        $this->title = $request['title'];

        return $this->convertToText();
    }

    public function convertToText() {
       
        if(isset($this->fileBs) && !file_exists($this->fileBs) &&
           isset($this->filePl) && !file_exists($this->filePl) &&
           isset($this->fileHis) && !file_exists($this->fileHis)
        ) {
            return view('submit', ['return' => 'File Not exists!']);
        }

        $fileExtBs = $this->fileBs->getClientOriginalExtension();
        $fileExtPl = $this->filePl->getClientOriginalExtension();
        $fileExtHis = $this->fileHis->getClientOriginalExtension();
 
        if($fileExtBs == "pdf" && $fileExtPl == "pdf" && $fileExtHis == "pdf") {
            return $this->readFReport();
            
        }else {
            return 'Invalid File Type, accepted file type : PDF';
            // return view('submit', ['return' => 'Invalid File Type, accepted file type : pdf/images']);
        }
    }

    public function readFReport(){

        //Rport

        //bs
        $bsinput = $this->readBsInput();
        
        $bsrestate = $this->readBsRestate();
        $bsoutput = $this->readBsOutput();
        //pl
        $plinput = $this->readPlInput();
        $ploutput = $this->readPlOutput();
        //his
        $hisinput = $this->readHis();
        //liquidity ratios
        $lRatios = $this->lRatios();
        //cap structure ratios
        $csRatio = $this->csRatio();
        //efficiency ratios
        $effRatio = $this->effRatio();
        //bank ratio
        $bankRatio = $this->bankRatio();
        //z score
        $zScore = $this->zScore();

        //Graph

        //Liquidity Graphs
        $liqGraph = $this->liqGraph();
        // //Cap Structure graphs
        $capGraph = $this->capGraph();
        // //Efficiency Graphs
        $effGraph = $this->effGraph();
        
        $fullReport = [
            'title' => $this->title,
            'bsinput' => $bsinput,
            'bsrestate' => $bsrestate,
            'bsoutput' => $bsoutput,
            'plinput' => $plinput,
            'ploutput' => $ploutput,
            'hisinput' => $hisinput,
            'lRatios' => $lRatios,
            'csRatio' => $csRatio,
            'effRatio' => $effRatio,
            'bankRatio' => $bankRatio,
            'zScore' => $zScore,
            'debEq' => $capGraph[0],
            'retainValue' => $capGraph[1],
            'workCap'=> $liqGraph[0],
            'ratioValue'=> $liqGraph[1],
            'outstandingValue' => $effGraph[0],
            'turValue' => $effGraph[1],
            'cycleValue' => $effGraph[2],
            'totalTur' => $effGraph[3],
            'fixTue' => $effGraph[4],
            'reEq' => $effGraph[5],
        ];

        $tableData = [
            'fullReport' => json_encode($fullReport)
        ];

        financeReport::create([
            'title' => $this->title,
            'value' => json_encode($fullReport),
            'file_type' => 'pdf'
        ]);
        
        return view('menu',$tableData);
        // return view('table',$tableData);
    }

    public function fullReport(){
        $tableData = json_decode($_POST['report'],true);
        if($_POST['type'] == 'full'){
            return view('table',$tableData);
        }else if($_POST['type'] == 'bsoutput'){
            return view('bsoutput',$tableData);
        }else if($_POST['type'] == 'ploutput'){
            return view('ploutput',$tableData);
        }else if($_POST['type'] == 'liqr'){
            return view('liqr',$tableData);
        }else if($_POST['type'] == 'capr'){
            return view('capr',$tableData);
        }else if($_POST['type'] == 'effr'){
            return view('effr',$tableData);
        }else if($_POST['type'] == 'bank'){
            return view('bank',$tableData);
        }else if($_POST['type'] == 'zscore'){
            return view('zscore',$tableData);
        }else if($_POST['type'] == 'liq'){
            return view('liq',$tableData);
        }else if($_POST['type'] == 'cap'){
            return view('cap',$tableData);
        }else if($_POST['type'] == 'eff'){
            return view('eff',$tableData);
        }
    }

    //func
    public function numberFormat($number){
        if($number == '-'){
            return 0;
        }else{
            $numberFormat=str_replace(",","",$number);
            $numberFormat=str_replace("(","-",$numberFormat);
            $numberFormat=str_replace(")","",$numberFormat);
            return $numberFormat;
        }
    }

    public function errOutput($title,$value){
        return $value.' not found at '.$title;
    }

    //graph
    
    public function effGraph(){
        $lRatio = $this->lRatios();
        $bsrestate = $this->readBsRestate();
        $getHead = $bsrestate[0][0];
        ///
        $effRatio = $this->effRatio();
        //liq ratio
        foreach ($effRatio as $inkey => $invalue) {
            //1
            if(array_key_exists("A/R Turnover",$invalue) || array_key_exists("A/RTurnover",$invalue)){
                $arTur = $invalue;
            }
            if(array_key_exists("A/P Turnover",$invalue) || array_key_exists("A/PTurnover",$invalue)){
                $apTur = $invalue;
            }
            if(array_key_exists("Inventory Turnover",$invalue) || array_key_exists("InventoryTurnover",$invalue)){
                $invTur = $invalue;
            }
            //2
            if(array_key_exists("Days A/R Outstanding",$invalue) || array_key_exists("DaysA/ROutstanding",$invalue)){
                $arOut = $invalue;
            }
            if(array_key_exists("Days Inventory Outstanding",$invalue) || array_key_exists("DaysInventoryOutstanding",$invalue)){
                $invOut = $invalue;
            }
            if(array_key_exists("Days A/P Outstanding",$invalue) || array_key_exists("DaysA/POutstanding",$invalue)){
                $apOut = $invalue;
            }
            //3
            if(array_key_exists("Operating Cycle (Days)",$invalue) || array_key_exists("OperatingCycle(Days)",$invalue)){
                $opCyc = $invalue;
            }
            if(array_key_exists("Cash Conversion Cycle",$invalue) || array_key_exists("Cash Conversion Cycle",$invalue)){
                $cashCyc = $invalue;
            }
            if(array_key_exists("Total Asset Turnover",$invalue) || array_key_exists("TotalAssetTurnover",$invalue)){
                $totTur = $invalue;
            }
            //
            if(array_key_exists("Fixed Asset Turnover",$invalue) || array_key_exists("FixedAssetTurnover",$invalue)){
                $fixTurValue = $invalue;
            }
            if(array_key_exists("Return on Equity",$invalue) || array_key_exists("ReturnonEquity",$invalue)){
                $reEqValue = $invalue;
            }
            
        }
        //Current and Acid Test Ratios
        $turValue = array();
        $turValue['ar'][0] = array();
        $turValue['ap'][0] = array();
        $turValue['inv'][0] = array();
        $turValue['title'] = $this->title.' A/R, A/P, and Inventory Turnover';
        $turValue['ar']['type'] = 'A/R Turnover';
        $turValue['ap']['type'] = 'A/P Turnover';
        $turValue['inv']['type'] = 'Inventory Turnover';

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($turValue['ar'][0],array("y"=> $arTur[$inkey], "label"=> $inkey));
                array_push($turValue['ap'][0],array("y"=> $apTur[$inkey], "label"=> $inkey));
                array_push($turValue['inv'][0],array("y"=> $invTur[$inkey], "label"=> $inkey));
            }
        }

        ///Working Capital
        $outstandingValue = array();
        $outstandingValue['ar'][0] = array();
        $outstandingValue['ap'][0] = array();
        $outstandingValue['inv'][0] = array();
        $outstandingValue['title'] = $this->title.' Days Outstanding A/R, Inventory, A/P';
        $outstandingValue['ar']['type'] = 'Days A/R Outstanding';
        $outstandingValue['ap']['type'] = 'Days Inventory Outstanding';
        $outstandingValue['inv']['type'] = 'Days A/P Outstanding';

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($outstandingValue['ar'][0],array("y"=> $arOut[$inkey], "label"=> $inkey));
                array_push($outstandingValue['ap'][0],array("y"=> $apOut[$inkey], "label"=> $inkey));
                array_push($outstandingValue['inv'][0],array("y"=> $invOut[$inkey], "label"=> $inkey));
            }
        }

        ///Operating and Cash Conversion Cycles
        $cycleValue = array();
        $cycleValue['op'][0] = array();
        $cycleValue['cash'][0] = array();
        $cycleValue['title'] = $this->title.' Operating and Cash Conversion Cycles';
        $cycleValue['op']['type'] = 'Operating Cycle (Days)';
        $cycleValue['cash']['type'] = 'Cash Conversion Cycle';

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($cycleValue['op'][0],array("y"=> $opCyc[$inkey], "label"=> $inkey));
                array_push($cycleValue['cash'][0],array("y"=> $cashCyc[$inkey], "label"=> $inkey));
            }
        }

        ///Total Asset Turnover
        $totalTur = array();
        $totalTur[0]['title'] = $this->title.' Total Asset Turnover';
        $totalTur[0]['type'] = 'Total Asset Turnover';
        $totalTur[1] = array();

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($totalTur[1],array("y"=> $totTur[$inkey], "label"=> $inkey));
            }
        }

        ///Fixed Asset Turnover
        $fixTur = array();
        $fixTur[0]['title'] = $this->title.' Fixed Asset Turnover';
        $fixTur[0]['type'] = 'Fixed Asset Turnover';
        $fixTur[1] = array();

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($fixTur[1],array("y"=> $fixTurValue[$inkey], "label"=> $inkey));
            }
        }

        ///reEq
        $reEq = array();
        $reEq[0]['title'] = $this->title.' Return on Equity';
        $reEq[0]['type'] = 'Return on Equity';
        $reEq[1] = array();

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($reEq[1],array("y"=> $reEqValue[$inkey], "label"=> $inkey));
            }
        }
        
            $effAll[0] = $outstandingValue;
            $effAll[1] = $turValue;
            $effAll[2] = $cycleValue; 
            $effAll[3] = $totalTur;
            $effAll[4] = $fixTur;
            $effAll[5] = $reEq;
            return $effAll;
    }

    public function liqGraph(){
        $lRatio = $this->lRatios();
        $bsrestate = $this->readBsRestate();
        $getHead = $bsrestate[0][0];
        //liq ratio
        foreach ($lRatio as $inkey => $invalue) {
            if(array_key_exists("Working Capital",$invalue) || array_key_exists("WorkingCapital",$invalue)){
                $workCapValue = $invalue;
            }
            if(array_key_exists("Current Ratio",$invalue) || array_key_exists("CurrentRatio",$invalue)){
                $curRatio = $invalue;
            }
            if(array_key_exists("Acid Test Ratio",$invalue) || array_key_exists("AcidTestRatio",$invalue)){
                $testRatio = $invalue;
            }
        }
        ///Working Capital
        $workCap = array();
        $workCap[0]['title'] = $this->title.' Working Capital';
        $workCap[0]['type'] = 'Working Capital';
        $workCap[1] = array();

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($workCap[1],array("label"=> $inkey, "y"=> $workCapValue[$inkey]));
            }
        }
        //Current and Acid Test Ratios
        $ratioValue = array();
        $ratioValue['cur'][0] = array();
        $ratioValue['test'][0] = array();
        $ratioValue['title'] = $this->title.' Current and Acid Test Ratios';
        $ratioValue['cur']['type'] = 'Current Ratios';
        $ratioValue['test']['type'] = 'Acid Test Ratios';

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($ratioValue['cur'][0],array("y"=> $curRatio[$inkey], "label"=> $inkey));
                array_push($ratioValue['test'][0],array("y"=> $testRatio[$inkey], "label"=> $inkey));
            }
        }
        $liq[0] = $workCap;
        $liq[1] = $ratioValue;
        return $liq;
    }
    //
    public function capGraph(){
        
        $bankRatio = $this->bankRatio();
        $csRatio = $this->csRatio();
        $bsrestate = $this->readBsRestate();
        $getHead = $bsrestate[0][0];
        
        //
        foreach ($bankRatio as $inkey => $invalue) {

            $noSpace = str_replace(" ","",$invalue['title']);
            if($noSpace === "DebttoEquityRatio"){
                $debEqValue = $invalue;
            }
           
        }

        //liq ratio
        foreach ($csRatio as $inkey => $invalue) {

            if(array_key_exists("Retained Earnings to Total Capital",$invalue) || array_key_exists("RetainedEarningstoTotalCapital",$invalue)){
                $retain = $invalue;
            }
           
        }
        
        ///Debt to Equity Ratio
        $debEq = array();
        $debEq[0]['title'] = $this->title.' Debt to Equity Ratio';
        $debEq[0]['type'] = 'Debt to Equity Ratio';
        $debEq[1] = array();

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($debEq[1],array("y"=> $debEqValue[$inkey-4], "label"=> $inkey));
            }
        }


        ///Retained Earnings to Total Capital
        $retainValue = array();
        $retainValue[0]['title'] = $this->title.' Retained Earnings to Total Capital';
        $retainValue[0]['type'] = 'Retained Earnings to Total Capital';
        $retainValue[1] = array();

        foreach($getHead as $inkey => $invalue){
            if($inkey !== 'title'){
                array_push($retainValue[1],array("y"=> $retain[$inkey], "label"=> $inkey));
            }
        }
        $cap[0] = $debEq;
        $cap[1] = $retainValue;
        return $cap;
    }
    
    //report
    public function zScore(){
        $lRatio = $this->lRatios();
        $csRatio = $this->csRatio();
        $bsrestate = $this->readBsRestate();
        $plinput = $this->readPlInput();
        $getHead = $bsrestate[0][0];
        // return $lRatio;
        //liq ratio
        foreach ($lRatio as $inkey => $invalue) {
            if(array_key_exists("Working Capital",$invalue) || array_key_exists("WorkingCapital",$invalue)){
                $workCap = $invalue;
            }
        }
        
        //pl
        foreach ($plinput as $inkey => $invalue) {
            $noSpace = str_replace(" ","",$invalue['title']);
            if($noSpace === 'Incomefromcontinuingoperations'){
                $incomeOp = [$inkey];
            }
            if($noSpace === 'Interestexpense'){
                $InEx = [$inkey];
            }
        }

        //bs
        foreach ($bsrestate as $key => $value) {
            foreach ($value as $inkey => $invalue) {
                $noSpace = str_replace(" ","",$invalue['title']);
                if($noSpace === 'Totalassets'){
                    $totalAss = [$key,$inkey];
                }
                if($noSpace === 'Retainedearnings/(deficit)'){
                    $retain = [$key,$inkey];
                }
                if($noSpace === 'Totalliabilities'){
                    $totalLia = [$key,$inkey];
                }
                 
                if($noSpace === "Totalstockholders'equity"){
                    $stockEquity = [$key,$inkey];
                }
            }
        }

        $headTitle = [
            ['title',"Z-Score"],
        ]; 
        //
        $scoringTable = [
            'Above 2.60'=>"Healthy",
            '2.60 to 1.10'=>"Gray area; bankruptcy cannot be predicted",
            'Below 1.10'=>"Bankruptcy likely",
        ]; 
        //
        foreach($headTitle as $key => $value){

            foreach($getHead as $inkey => $invalue){
                if($inkey !== 'title'){
                    //bs
                    $getTotalAss=$this->numberFormat($bsrestate[$totalAss[0]][$totalAss[1]][$inkey]);
                    $getRetain=$this->numberFormat($bsrestate[$retain[0]][$retain[1]][$inkey]);
                    $getTotalLia=$this->numberFormat($bsrestate[$totalLia[0]][$totalLia[1]][$inkey]);
                    $getStockEquity=$this->numberFormat($bsrestate[$stockEquity[0]][$stockEquity[1]][$inkey]);

                    //pl
                    $getIncomeOp=$this->numberFormat($plinput[$incomeOp[0]][$inkey]);
                    $getInEx=$this->numberFormat($plinput[$InEx[0]][$inkey]);
                    if($key == 0){
                        $zScore[$key][$inkey-4] = number_format((6.56*$workCap[$inkey]/$getTotalAss)+(3.26*$getRetain/$getTotalAss)+(6.72*($getIncomeOp+$getInEx)/$getTotalAss)+(1.05*$getStockEquity/$getTotalLia), 2, '.', '');
                    }else{
                        $zScore[$key][$inkey-4] = "";
                    }

                }else{
                    $zScore[$key][$value[0]] = $value[1];
                }
                
                
            }
            
        }
        $zScoreTable[0] = $scoringTable;
        $zScoreTable[1] = $zScore[0];

        return $zScoreTable;
    }
    public function bankRatio(){
        $lRatio = $this->lRatios();
        $csRatio = $this->csRatio();
        $bsrestate = $this->readBsRestate();
        $getHead = $bsrestate[0][0];
        
        //liq ratio
        foreach ($lRatio as $inkey => $invalue) {
            if(array_key_exists("Current Ratio",$invalue) || array_key_exists("CurrentRatio",$invalue)){
                $curRatio = $invalue;
            }
             

            if(array_key_exists("Working Capital",$invalue) || array_key_exists("WorkingCapital",$invalue)){
                $workCap = $invalue;
            }

            if(array_key_exists("Debt Coverage Ratio",$invalue) || array_key_exists("DebtCoverageRatio",$invalue) || array_key_exists("Debt CoverageRatio",$invalue) || array_key_exists("DebtCoverage Ratio",$invalue)){
                $debCoverage = $invalue;
            }
            
        }
        //cap s ratio
        foreach ($csRatio as $inkey => $invalue) {
            if(array_key_exists("Debt to Equity",$invalue) || array_key_exists("DebttoEquity",$invalue)){
                $debEq = $invalue;
            }
        }

        //bs
        foreach ($bsrestate as $key => $value) {
            foreach ($value as $inkey => $invalue) {
                $noSpace = str_replace(" ","",$invalue['title']);
                if($noSpace === 'Retainedearnings/(deficit)'){
                    $retain = [$key,$inkey];
                }
            }
        }

        $headTitle = [
            ['title',"Current Ratio"],
            ['title',"Tangible Net Worth"],
            ['title',"Debt to Equity Ratio"],
            ['title',"Working Capital"],
            ['title',"Debt Coverage Ratio"],
        ]; 

        foreach($headTitle as $key => $value){

            foreach($getHead as $inkey => $invalue){
                if($inkey !== 'title'){
                    // bs
                    $getRetain=$this->numberFormat($bsrestate[$retain[0]][$retain[1]][$inkey]);
                    // cap
                    
                    if($key == 0){
                        $bankRatio[$key]["Bankers' Requirement"] = '1.00';
                        $bankRatio[$key][$inkey-4] = $curRatio[$inkey];
                        // $bankRatio[$key][$inkey] = number_format($getCurrentLia/$getStockEquity, 2, '.', '');
                    }elseif($key == 1){
                        $bankRatio[$key]["Bankers' Requirement"] = '300,000';
                        $bankRatio[$key][$inkey-4] = $getRetain;
                        // $bankRatio[$key][$inkey] = number_format($getCurrentLia/$getStockEquity, 2, '.', '');
                    }elseif($key == 2){
                        $bankRatio[$key]["Bankers' Requirement"] = '4.00';
                        $bankRatio[$key][$inkey-4] = $debEq[$inkey];
                        // $bankRatio[$key][$inkey] = number_format($getCurrentLia/$getStockEquity, 2, '.', '');
                    }elseif($key == 3){
                        $bankRatio[$key]["Bankers' Requirement"] = '250,000';
                        $bankRatio[$key][$inkey-4] = $workCap[$inkey];
                        // $bankRatio[$key][$inkey] = number_format($getCurrentLia/$getStockEquity, 2, '.', '');
                    }elseif($key == 4){
                        $bankRatio[$key]["Bankers' Requirement"] = '1.20';
                        $bankRatio[$key][$inkey-4] = $debCoverage[$inkey-4];
                        // $bankRatio[$key][$inkey] = number_format($getCurrentLia/$getStockEquity, 2, '.', '');
                    }else{
                        $bankRatio[$key]["Bankers' Requirement"] = '1.00';
                        $bankRatio[$key][$inkey-4] = "";
                    }

                }else{
                    $bankRatio[$key][$value[0]] = $value[1];
                }
                
                
            }
            
        }

        return $bankRatio;
    }

    public function effRatio(){
        $bsrestate = $this->readBsRestate();
        $getHead = $bsrestate[0][0];
        $plinput = $this->readPlInput();
        $hisinput = $this->readHis();
        
        $headTitle = [
            ['A/R Turnover',"Credit Sales / Average Accounts Receivable"],
            ['Days A/R Outstanding',"365 / Accounts Receivable Turnover"],
            ['Inventory Turnover',"Cost of Goods Sold / Average Inventory"],
            ['Days Inventory Outstanding','365 / Inventory Turnover'],
            ['Operating Cycle (Days)','Days Accounts Receivable Outstanding + Days Inventory Outstanding'],
            ['A/P Turnover','Annual Purchases / Average Accounts Payable'],
            ['Days A/P Outstanding','365 / A/P Turnover'],
            ['Cash Conversion Cycle','Operating cycle - Days A/P outstanding'],
            ['Total Asset Turnover','Net Revenue / Total assets'],
            ['Fixed Asset Turnover','Net Revenue / Net Fixed Assets'],
            ['Return on Equity',"Net Income / Stockholders' Equity"],
            ['Return on capital employed',"Net Income / (Stockholders' Equity - Nonoperating Assets (i.e. investments))"]
        ];

        //find var
        //bs
        
        foreach ($bsrestate as $key => $value) {
            
            foreach ($value as $inkey => $invalue) {
                $noSpace = str_replace(" ","",$invalue['title']);
                if($noSpace === 'Property&equipment'){
                    $bsProNe = [$key,$inkey];
                }
                if($noSpace === 'Totalassets'){
                    $totalAss = [$key,$inkey];
                }
                if($noSpace === "Totalstockholders'equity"){
                    $totalStock = [$key,$inkey];
                }
                
            }
        }
        //pl
        foreach ($plinput as $inkey => $invalue) {
            $noSpace = str_replace(" ","",$invalue['title']);

            if($noSpace === 'Revenues'){
                $rev = [$inkey];
            }
            if($noSpace === 'Costofgoodssold'){
                $costSold = [$inkey];
            }
            if($noSpace === 'Netincome(loss)'){
                $netInc = [$inkey];
            }
           
            
        }
        //his
        foreach ($hisinput as $key => $value) {

            foreach ($value as $inkey => $invalue) {

                if(count($invalue) > 1){
                    if($invalue['title'] === 'A/R'){
                        if(isset($invalue['YTD Avg.'])){
                            $hisAR[$value[0]] = $invalue['YTD Avg.'];
                        }else if(isset($invalue['YTDAvg.'])){
                            $hisAR[$value[0]] = $invalue['YTDAvg.'];
                        }
                        $hisARDec[$value[0]] = $invalue['Dec-'.substr( $value[0], -2 )];
                    }else if($invalue['title'] === 'Inventory'){
                        $hisInv[$value[0]] = $invalue['YTD Avg.'];
                        $hisJan[$value[0]] = $invalue['Jan-'.substr( $value[0], -2 )];
                        $hisDec[$value[0]] = $invalue['Dec-'.substr( $value[0], -2 )];

                        if(isset($hisinput[$key-1])){
                            $hisPrevDec[$value[0]] = $hisinput[$key-1][$inkey]['Dec-'.substr( $value[0]-1, -2 )];
                        }else{
                            $hisPrevDec[$value[0]] = $invalue['Dec-'.substr( $value[0], -2 )];
                        }
                        
                    }else if($invalue['title'] === 'A/P'){
                        if(isset($invalue['YTD Avg.'])){
                            $hisAP[$value[0]] = $invalue['YTD Avg.'];
                        }else if(isset($invalue['YTDAvg.'])){
                            $hisAP[$value[0]] = $invalue['YTDAvg.'];
                        }
                    }else if($invalue['title'] === 'AccruedPay.'){
                        if(isset($invalue['YTD Avg.'])){
                            $accPay[$value[0]] = $invalue['YTD Avg.'];
                        }else if(isset($invalue['YTDAvg.'])){
                            $accPay[$value[0]] = $invalue['YTDAvg.'];
                        }
                    }else if($invalue['title'] === 'Accrued Pay.'){
                        if(isset($invalue['YTD Avg.'])){
                            $accPay[$value[0]] = $invalue['YTD Avg.'];
                        }else if(isset($invalue['YTDAvg.'])){
                            $accPay[$value[0]] = $invalue['YTDAvg.'];
                        }
                        
                    }
                }
            }
        }

        $i5=0;
        $i6=0;
        $i7=0;
        foreach($headTitle as $key => $value){
            
            foreach($getHead as $inkey => $invalue){
                if($inkey !== 'title'){
                    //pl
                    $noSpaceRev = $this->numberFormat($plinput[$rev[0]][$inkey]);
                    $getHisAR=$this->numberFormat($hisAR[$inkey]);
                    //his
                    $getCostSold = $this->numberFormat($plinput[$costSold[0]][$inkey]);
                    $getHisInv=$this->numberFormat($hisInv[$inkey]);
                    //
                    $getHisJan=$this->numberFormat($hisJan[$inkey]);
                    $getHisDec=$this->numberFormat($hisDec[$inkey]);
                    //
                    $getHisAP=$this->numberFormat($hisAP[$inkey]);
                    $getAccPay=$this->numberFormat($accPay[$inkey]);
                    //
                    $getHisPrevDec=$this->numberFormat($hisPrevDec[$inkey]);
                    //
                    $getHisARDec=$this->numberFormat($hisARDec[$inkey]);

                    $getnetInc = $this->numberFormat($plinput[$netInc[0]][$inkey]);

                    //bs
                    $getBsProNe=$this->numberFormat($bsrestate[$bsProNe[0]][$bsProNe[1]][$inkey]);
                    $getTotalAss=$this->numberFormat($bsrestate[$totalAss[0]][$totalAss[1]][$inkey]);
                    $getTotalStock=$this->numberFormat($bsrestate[$totalStock[0]][$totalStock[1]][$inkey]);

                    if($key == 0){
                        $csRatio[$key]['Goal'] = [$inkey+1,'15.00'];
                        $csRatio[$key][$inkey] = number_format($noSpaceRev/$getHisAR, 2, '.', '');
                    }else if($key == 1){
                        $csRatio[$key]['Goal'] = [$inkey+1,'41.00'];
                        $csRatio[$key][$inkey] = number_format(365/($noSpaceRev/$getHisAR), 2, '.', '');
                    }else if($key == 2){
                        $csRatio[$key]['Goal'] = [$inkey+1,'61.00'];
                        $csRatio[$key][$inkey] = number_format($getCostSold/$getHisInv, 2, '.', '');
                    }else if($key == 3){
                        $csRatio[$key]['Goal'] = [$inkey+1,'41.00'];
                        $csRatio[$key][$inkey] = number_format(365/($getCostSold/$getHisInv), 2, '.', '');
                    }else if($key == 4){
                        $csRatio[$key]['Goal'] = [$inkey+1,'55.00'];
                        $csRatio[$key][$inkey] = number_format(365/($noSpaceRev/$getHisAR)+365/($getCostSold/$getHisInv), 2, '.', '');
                    }else if($key == 5){
                        $i5++;
                        $csRatio[$key]['Goal'] = [$inkey+1,'17.00'];
                        if($i5 == 1){
                            $csRatio[$key][$inkey] = number_format((($getCostSold-$getHisJan)+$getHisDec)/($getHisAP+$getAccPay), 2, '.', '');
                        }elseif($i5 == 2 || $i5 == 3){
                            $csRatio[$key][$inkey] = number_format( (($getCostSold-$getHisPrevDec)+$getHisDec)/($getHisAP+$getAccPay), 2, '.', '');
                        }elseif($i5 == 4){
                            $csRatio[$key][$inkey] = number_format( (($getCostSold-$getHisPrevDec)+$getHisARDec)/($getHisAP+$getAccPay), 2, '.', '');
                        }

                    }else if($key == 6){
                        $i6++;
                        $csRatio[$key]['Goal'] = [$inkey+1,'40.00'];
                        if($i6 == 1){
                            $csRatio[$key][$inkey] = number_format(365/number_format((($getCostSold-$getHisJan)+$getHisDec)/($getHisAP+$getAccPay), 2, '.', ''), 2, '.', '');
                        }elseif($i6 == 2 || $i6 == 3){
                            $csRatio[$key][$inkey] = number_format(365/number_format((($getCostSold-$getHisPrevDec)+$getHisDec)/($getHisAP+$getAccPay), 2, '.', ''), 2, '.', '');
                        }elseif($i6 == 4){
                            $csRatio[$key][$inkey] = number_format(365/number_format((($getCostSold-$getHisPrevDec)+$getHisARDec)/($getHisAP+$getAccPay), 2, '.', ''), 2, '.', '');
                        }

                    }else if($key == 7){
                        $i7++;
                        $csRatio[$key]['Goal'] = [$inkey+1,''];
                        if($i7 == 1){
                            $csRatio[$key][$inkey] = number_format(number_format(365/($noSpaceRev/$getHisAR)+365/($getCostSold/$getHisInv), 2, '.', '')-number_format(365/number_format((($getCostSold-$getHisJan)+$getHisDec)/($getHisAP+$getAccPay), 2, '.', ''), 2, '.', ''), 2, '.', '');
                        }elseif($i7 == 2 || $i7 == 3){
                            $csRatio[$key][$inkey] = number_format(number_format(365/($noSpaceRev/$getHisAR)+365/($getCostSold/$getHisInv), 2, '.', '')-number_format(365/number_format((($getCostSold-$getHisPrevDec)+$getHisDec)/($getHisAP+$getAccPay), 2, '.', ''), 2, '.', ''), 2, '.', '');
                        }elseif($i7 == 4){
                            $csRatio[$key][$inkey] = number_format(number_format(365/($noSpaceRev/$getHisAR)+365/($getCostSold/$getHisInv), 2, '.', '')-number_format(365/number_format((($getCostSold-$getHisPrevDec)+$getHisARDec)/($getHisAP+$getAccPay), 2, '.', ''), 2, '.', ''), 2, '.', '');
                        }

                    }else if($key == 8){
                        $csRatio[$key]['Goal'] = [$inkey+1,''];
                        $csRatio[$key][$inkey] = number_format($noSpaceRev/$getTotalAss, 2, '.', '');
                    }else if($key == 9){
                        $csRatio[$key]['Goal'] = [$inkey+1,''];
                        $csRatio[$key][$inkey] = number_format($noSpaceRev/$getBsProNe, 2, '.', '');
                    }else if($key == 10){
                        $csRatio[$key]['Goal'] = [$inkey+1,''];
                        $csRatio[$key][$inkey] = number_format($getnetInc/$getTotalStock, 2, '.', '');
                    }else{
                        $csRatio[$key]['Goal'] = $inkey+1;
                        $csRatio[$key][$inkey] = '';
                    }
                }else{
                    $csRatio[$key][$value[0]] = $value[1];
                }
            }
            
        }

        return $csRatio;
    }

    public function csRatio(){
        $bsrestate = $this->readBsRestate();
        $getHead = $bsrestate[0][0];
        
        //find var
        //[key,title] 
        foreach ($bsrestate as $key => $value) {

            foreach ($value as $inkey => $invalue) {
                $noSpace = str_replace(" ","",$invalue['title']);
                if($noSpace === 'Totalliabilities'){
                    $totalLia = [$key,$inkey];
                }
                if($noSpace === "Totalstockholders'equity"){
                    $stockEquity = [$key,$inkey];
                }
                if($noSpace === "Notepayabletobank-current"){
                    $bankCur = [$key,$inkey];
                }
                if($noSpace === "Notepayabletobank-long-term"){
                    $bankLong = [$key,$inkey];
                }
                if($noSpace === "Retainedearnings/(deficit)"){
                    $retain = [$key,$inkey];
                }
            }
        }

        $headTitle = [
            ['Debt to Equity',"Total Liabilities / Stockholders' Equity"],
            ['Debt to Total Capitalization',"Debt / Debt + Stockholders' Equity"],
            ['Retained Earnings to Total Capital',"Retained Earnings / Stockholders' Equity"],
        ]; 

        foreach($headTitle as $key => $value){

            foreach($getHead as $inkey => $invalue){
                if($inkey !== 'title'){
                    //0
                    $getCurrentLia=$this->numberFormat($bsrestate[$totalLia[0]][$totalLia[1]][$inkey]);
                    $getStockEquity=$this->numberFormat($bsrestate[$stockEquity[0]][$stockEquity[1]][$inkey]);
                    //1
                    $getBankCur=$this->numberFormat($bsrestate[$bankCur[0]][$bankCur[1]][$inkey]);
                    $getBankLong=$this->numberFormat($bsrestate[$bankLong[0]][$bankLong[1]][$inkey]);
                    //2
                    $getRetain=$this->numberFormat($bsrestate[$retain[0]][$retain[1]][$inkey]);
                  
                    if($key == 0){
                        $csRatio[$key]['Goal'] = [$inkey+1,'3.00'];
                        $csRatio[$key][$inkey] = number_format($getCurrentLia/$getStockEquity, 2, '.', '');
                    }else if($key == 1){
                        $csRatio[$key]['Goal'] = [$inkey+1,'0.40'];
                        $csRatio[$key][$inkey] = number_format(($getBankCur+$getBankLong)/($getBankCur+$getBankLong+$getStockEquity), 2, '.', '');;
                    }else if($key == 2){
                        $csRatio[$key]['Goal'] = [$inkey+1,'0.10'];
                        
                        $csRatio[$key][$inkey] = number_format($getRetain/$getStockEquity, 2, '.', '');
                        // $csRatio[$key][$inkey] = number_format($getRetain/$getStockEquity, 2, '.', '');
                    }else{
                        $csRatio[$key]['Goal'] = $inkey+1;
                        $csRatio[$key][$inkey] = "";
                    }

                }else{
                    $csRatio[$key][$value[0]] = $value[1];
                }
                
                
            }
            
        }

        return $csRatio;
    }
    
    public function lRatios(){
        $bsrestate = $this->readBsRestate();
        $plinput = $this->readPlInput();
        $hisinput = $this->readHis();
        $getHead = $bsrestate[0][0];
        
        //find var
        //[key,title] 
        foreach ($bsrestate as $key => $value) {
            
            foreach ($value as $inkey => $invalue) {
                $noSpace = str_replace(" ","",$invalue['title']);
                if($noSpace === 'Totalcurrentassets'){
                    $currentAss = [$key,$inkey];
                }
                if($noSpace === 'Totalcurrentliabilities'){
                    $currentLia = [$key,$inkey];
                }
                if($noSpace === 'Cash'){
                    $cash = [$key,$inkey];
                }
                if($noSpace === 'Accountsreceivable'){
                    $accRec = [$key,$inkey];
                }
                if($noSpace === 'Inventory'){
                    $inv = [$key,$inkey];
                }
                
            }
        }
        //pl
        foreach ($plinput as $inkey => $invalue) {
            $noSpace = str_replace(" ","",$invalue['title']);
            if($noSpace === 'Operatingincome'){
                $OpInc = [$inkey];
            }
            if($noSpace === 'Interestexpense'){
                $InEx = [$inkey];
            }
            if($noSpace === 'Depreciation'){
                $dep = [$inkey];
            }
            if($noSpace === 'Revenues'){
                $rev = [$inkey];
            }
        }
        //
        //his
        foreach ($hisinput as $key => $value) {
            foreach ($value as $inkey => $invalue) {
                if(count($invalue) > 1){
                    if(isset($invalue['YTDTotal'])){
                        $hisTotal[$value[0]] = $invalue['YTDTotal'];
                    }
                    
                }
            }
        }

        //lia ->$bsrestate[$currentLia[0]][$currentLia[1]];
        //ass ->$bsrestate[$currentAss[0]][$currentAss[1]];

        //1 asset-lia
        $headTitle = [
                    ['Working Capital','Current Assets - Current Liabilities'],
                    ['Working Capital Turnover','Annual Sales / Working Capital'],
                    ['Current Ratio','Current Assets / Current Liabilities'],
                    ['Acid Test Ratio','Quick assets (cash + accounts receivable) / Current Liabilities'],
                    ['Inventory to Working Capital','Inventory / Working Capital'],
                    ['Times Interest Earned','Net Income / Interest Expense'],
                    ['Debt Coverage Ratio','EBITDA / Interest + (Principal / 1 - Tax Rate)'],
                ]; 

        foreach($headTitle as $key => $value){

            foreach($getHead as $inkey => $invalue){
                if($inkey !== 'title'){
                    //get value
                    $curAss=$this->numberFormat($bsrestate[$currentAss[0]][$currentAss[1]][$inkey]);
                    $curLia=$this->numberFormat($bsrestate[$currentLia[0]][$currentLia[1]][$inkey]);

                    //
                    $getCash=$this->numberFormat($bsrestate[$cash[0]][$cash[1]][$inkey]);
                    $getAccRec=$this->numberFormat($bsrestate[$accRec[0]][$accRec[1]][$inkey]);
                    
                    //
                    $noSpaceRev = $this->numberFormat($plinput[$rev[0]][$inkey]);

                    //
                    $getInv=$this->numberFormat($bsrestate[$inv[0]][$inv[1]][$inkey]);
                    $getOpInc=$this->numberFormat($plinput[$OpInc[0]][$inkey]);
                    $getInEx=$this->numberFormat($plinput[$InEx[0]][$inkey]);
                    $getDep=$this->numberFormat($plinput[$dep[0]][$inkey]);
                    $getHisTotal=$this->numberFormat($hisTotal[$inkey]);
                    //0
                    if($key == 0){
                        $lRatio[$key]['Goal'] = [$inkey+1,2500000];
                        
                        $lRatio[$key][$inkey] = $curAss-$curLia;
                    }else if($key == 1){
                        $lRatio[$key]['Goal'] = [$inkey+1,25];
                        
                        $lRatio[$key][$inkey] = number_format($noSpaceRev/($curAss-$curLia), 2, '.', '');
                    }else if($key == 2){
                        $lRatio[$key]['Goal'] = [$inkey+1,'2.00'];
                        $lRatio[$key][$inkey] = number_format($curAss/$curLia, 2, '.', '');
                    }else if($key == 3){
                        $lRatio[$key]['Goal'] = [$inkey+1,'1.00'];
                        $lRatio[$key][$inkey] = number_format(($getAccRec+$getCash)/$curLia, 2, '.', '');
                    }else if($key == 4){
                        $lRatio[$key]['Goal'] = [$inkey+1,'2.50'];
                        $lRatio[$key][$inkey] = number_format($getInv/($curAss-$curLia), 2, '.', '');
                    }else if($key == 5){
                        $lRatio[$key]['Goal'] = [$inkey+1-4,'5.50'];
                        $lRatio[$key][$inkey-4] = number_format($getOpInc/$getInEx, 2, '.', '');
                    }else if($key == 6){
                        $lRatio[$key]['Goal'] = [$inkey+1-4,'2.50'];
                        $lRatio[$key][$inkey-4] = number_format(($getOpInc+$getDep)/($getInEx+($getHisTotal/(1-0.35))), 2, '.', '');
                        // $hisTotal
                        //(Operating income+Depreciation)/(Interest expense+('his[Debt Repayment][YTD Total]/(1-0.35)))
                        //(20+16)/(23+('his 13/(1-0.35)))
                        //($getOpInc+$getDep)/($getInEx+($hisTotal[$inkey]/(1-0.35)))
                    }else{
                        $lRatio[$key]['Goal'] = $inkey+1;
                        $lRatio[$key][$inkey] = "";
                    }
                    //$inv
                    //2
                    
                }else{
                    $lRatio[$key][$value[0]] = $value[1];
                }
                
                
            }
        }

        return $lRatio;
    }
    //bs
    //
    //
    //
    public function readBsInput(){
        $string = null;
        $toText = (new Pdf($this->pdf_path))
        ->setPdf($this->fileBs)
        ->setOptions(['layout', 'r 96'])
        ->addOptions(['f 1'])
        ->text();

        $textToArr = explode("\n",$toText);
        
        foreach ($textToArr as $key => $value) {
            $string[$key] = str_replace('  ', '$', $value);
        }
       
        foreach ($string as $key => $value) {
            $explodeArr[$key] = explode("$",$value);
        }
        
        foreach ($explodeArr as $key => $value) {
            $removeEmp[$key] = array_values(array_filter($value));   
        }
        $removeEmpArr = array_values(array_filter($removeEmp));
        
        
        foreach ($removeEmpArr as $key => $value) {
            $removeSpace[$key] = preg_replace('/ /', '', $value, 1);
        }
        
        foreach ($removeSpace as $key => $value) {
            if(count($value) == 1){
                $reSpace = str_replace(" ","",$value[0]);
                if ($reSpace == 'Currentliabilities' || $reSpace == 'currentliabilities') {
                    $liaCount = $key;
                }
                $removeArr[$key] = [];
            }else{
                $removeArr[$key] = $value;
            }
        }
            
        $getAsset = array_slice($removeArr,0,$liaCount);
        $getLia = array_slice($removeArr,$liaCount);
        
        $removeArrEmp[0] = array_values(array_filter($getAsset));
        $removeArrEmp[1] = array_values(array_filter($getLia));
        
        $getAll[0] = $removeArrEmp[0];
        array_unshift($removeArrEmp[1],$removeArrEmp[0][0]);
        $getAll[1] = $removeArrEmp[1];
        
        foreach ($getAll as $getAllKey => $getAllValue) {
            $removeArrEmp = array_values(array_filter($getAllValue));
            
            //t2 checkpoint
            $mainHead=$removeArrEmp[0];
            array_unshift($mainHead,'title');
            $mainAllBody = array_slice($removeArrEmp,1);

            //combine head & body 
            foreach($mainAllBody as $bKey => $bValue){
                foreach ($mainHead as $hKey => $hValue) {
                    if($hValue == ""){
                        //title
                        $finalAll[$getAllKey][$bKey][$hValue] = $bValue[$hKey];
                    }else{
                        //body
                        //$bValue[$hKey-1]
                        $filterHead = $mainHead;
                        $finalAll[$getAllKey][$bKey][$hValue] = '';
                    }
                }
            }

            foreach($mainAllBody as $bKey => $bValue){
                // return count($bValue);
                $bodyCount = count($bValue);
                $headCount = count($filterHead);

                foreach ($mainHead as $hKey => $hValue) {
                    if($hValue !== "" && $hValue !== "Note"){
                        //body
                        $finalAll[$getAllKey][$bKey][$filterHead[$headCount-$hKey-1]] = $bValue[$bodyCount-$hKey-1];
                    }
                }
            }
        }
        //bsinput
        
        return $finalAll;
    }
    //
    //
    //
    //

    public function readBsRestate(){

        $bsrestate = $this->readBsInput();
    
        foreach ($bsrestate[1] as $key => $value) {
            // return $value;
            foreach ($value as $inkey => $invalue) {

                if(strpos($invalue, 'shareholders') !== false || strpos($invalue,"subordinated") !== false){
                    //move to get lia + 1
                    $toMove[] = $value;
                    $bsrestate[1][$key] = [];
                }

                $invalue = str_replace(" ","",$invalue);
                if(strpos($invalue, 'Totalliabilities') !== false){
                    //get [0]
                    $getTotalLia[] = $key;
                }
                if(strpos($invalue, 'Totalcurrentliabilities') !== false){
                    $getCurrentLia = $key;
                }

                if(strpos($invalue, 'Totalstockholders') !== false){
                    $getStockholders = $key;
                }

            }
        }
        // 8($getCurrentLia) + - 12($getTotalLia[0]-1)

        //clear total lia
        foreach ($bsrestate[1][$getTotalLia[0]] as $key => $value) {
            if($key !== 'title'){
                $bsrestate[1][$getTotalLia[0]][$key] = 0;
            }
        }
        //add lia (getCurrentLia to getTotalLia)
        for ($i=$getCurrentLia; $i < $getTotalLia[0]; $i++) { 
        
            foreach ($bsrestate[1][$i] as $key => $value) {
                //$bsrestate[1][$getTotalLia[0]]=;
                if($key !== 'title'){
                    $smallValue = str_replace(",","",$value);
                    if($value !== '-'){
                        $bsrestate[1][$getTotalLia[0]][$key] += $smallValue;
                    }
                }
            
            }
        }

        //15($getTotalLia[0]+2) to 18($getStockholders-1)
        //clear total stockholder
        foreach ($bsrestate[1][$getStockholders] as $key => $value) {
            if($key !== 'title'){
                $bsrestate[1][$getStockholders][$key] = 0;
            }
        }

        array_splice($bsrestate[1],$getTotalLia[0]+2,0,$toMove);
        $bsrestate[1] = array_values(array_filter($bsrestate[1]));

        //add lia ($getTotalLia[0]+2 to getStockholders)
        for ($i=$getTotalLia[0]; $i < $getStockholders; $i++) { 
            // $test[] = $i;
            foreach ($bsrestate[1][$i] as $key => $value) {
            
                //$bsrestate[1][$getStockholders[0]]=;
                if($key !== 'title'){
                    $smallValue = str_replace(",","",$value);
                
                    if($value !== '-'){
                        if(strpos($value,"(") === 0){
                            $smallValue = str_replace("(","",$smallValue);
                            $smallValue = str_replace(")","",$smallValue);
                            $bsrestate[1][$getStockholders][$key] -= $smallValue;
                        }else{
                            $bsrestate[1][$getStockholders][$key] += $smallValue;
                        }

                    }
                }
            
            }
        }

        // *bsrestate result
        return $bsrestate; 
    }
    //
    //
    //
    //
    //
    public function readBsOutput(){
        $bsrestate = $this->readBsRestate();

        // bsoutput
        foreach($bsrestate as $outputKey => $outputValue) {
            $getLastCount = count($outputValue) - 1;

            foreach ($outputValue as $key => $value) {

                foreach ($value as $inkey => $invalue) {
                    if($inkey !== 'title'){
                        $smallValue = str_replace(",","",$invalue);
                        $smallValue = str_replace("(","",$smallValue);
                        $smallValue = str_replace(")","",$smallValue);
                        ////get last %
                        $allValue = str_replace(",","",$outputValue[$getLastCount][$inkey]);
                        $bsOutput[$outputKey][$key][$inkey] = $invalue;
                        if($smallValue === '-'){
                            $bsOutput[$outputKey][$key][$inkey.' %'] = "0.0"."%";
                        }else if(strpos($invalue,"(") === 0){
                            is_numeric($smallValue) ? 
                            $bsOutput[$outputKey][$key][$inkey.' %'] = '-'.number_format($smallValue/$allValue * 100, 1, '.', '')."%"
                            : 
                            $bsOutput[$outputKey][$key][$inkey.' %'] = '-';
                        }else{
                            is_numeric($smallValue) ? 
                            $bsOutput[$outputKey][$key][$inkey.' %'] = number_format($smallValue/$allValue * 100, 1, '.', '')."%"
                            : 
                            $bsOutput[$outputKey][$key][$inkey.' %'] = '-';
                         }
                    }else{
                        $bsOutput[$outputKey][$key][$inkey] = $invalue;
                    }
                }

            }
        }
        // *bsoutput result
        return $bsOutput;

    }
    //
    //
    //
    //

//pl
public function readPlInput(){
    $count = 0;
    $string = null;
    $toText = (new Pdf($this->pdf_path))
    ->setPdf($this->filePl)
    ->setOptions(['layout', 'r 96'])
    ->addOptions(['f 1'])
    ->text();

    $textToArr = explode("\n",$toText);    
    foreach ($textToArr as $key => $value) {
        $string[$key] = str_replace('  ', '$', $value);
    }
    foreach ($string as $key => $value) {
        $explodeArr[$key] = explode("$",$value);
    }
    foreach ($explodeArr as $key => $value) {
        $removeEmp[$key] = array_values(array_filter($value));   
    }
    $removeEmpArr = array_values(array_filter($removeEmp));
  
    foreach ($removeEmpArr as $key => $value) {
        $removeSpace[$key] = preg_replace('/ /', '', $value, 1);
    } 
    foreach ($removeSpace as $key => $value) {
        if(count($value) == 1){
            $removeArr[$key] = [];
        }else{
            $removeArr[$key] = $value;
        }
    }
    
    $removeArrEmp = array_values(array_filter($removeArr));     
    //t2 checkpoint
    $mainHead=$removeArrEmp[0];
    array_unshift($mainHead,'title');
    $mainAllBody = array_slice($removeArrEmp,1);

    //combine head & body 
    foreach($mainAllBody as $bKey => $bValue){
        foreach ($mainHead as $hKey => $hValue) {
            if($hValue == ""){
                //title
                $finalAll[$bKey][$hValue] = $bValue[$hKey];
            }else{
                //body
                //$bValue[$hKey-1]
                $filterHead = $mainHead;
                $finalAll[$bKey][$hValue] = '';
            }
        }
    }
                
    foreach($mainAllBody as $bKey => $bValue){
        // return count($bValue);
        $bodyCount = count($bValue);
        $headCount = count($filterHead);
        
        foreach ($mainHead as $hKey => $hValue) {
            
            if($hValue !== "" && $hValue !== "Note"){
                //body
                $finalAll[$bKey][$filterHead[$headCount-$hKey-1]] = $bValue[$bodyCount-$hKey-1];
            }
                
        }
        
    }
    //*plinput reuslt
    return $finalAll;

    }

//pl
public function readPlOutput(){
    $finalAll = $this->readPlInput();
    foreach ($finalAll as $key => $value) {
        
        foreach ($value as $inkey => $invalue) {
            if($inkey !== 'title'){
                $smallValue = str_replace(",","",$invalue);
                $smallValue = str_replace("(","",$smallValue);
                $smallValue = str_replace(")","",$smallValue);
                //get first %
                $allValue = str_replace(",","",$finalAll[0][$inkey]);
                $bsOutput[$key][$inkey] = $invalue;
                if($smallValue === '-'){
                    $bsOutput[$key][$inkey.' %'] = "0.0"."%";
                }else if(strpos($invalue,"(") === 0){
                    is_numeric($smallValue) ? 
                    $bsOutput[$key][$inkey.' %'] = '-'.number_format($smallValue/$allValue * 100, 1, '.', '')."%"
                    // $bsOutput[$key][$inkey.' %'] = $smallValue."isNEG".$allValue
                    : 
                    // $bsOutput[$key][$inkey.' %'] = '-';
                    $bsOutput[$key][$inkey.' %'] = '-';
                }else{
                    is_numeric($smallValue) ? 
                    // $bsOutput[$key][$inkey.' %'] = $smallValue."isPOS".$allValue
                    $bsOutput[$key][$inkey.' %'] = number_format($smallValue/$allValue * 100, 1, '.', '')."%"
                    : 
                    $bsOutput[$key][$inkey.' %'] = '-';
                    
                    // $bsOutput[$key][$inkey.' %'] = $smallValue."=".$allValue;
                 }
            }else{
                $bsOutput[$key][$inkey] = $invalue;
            }
        }
        
    }
    // *bsoutput result
    return $bsOutput;
    }


    //HIS
    public function readHis(){
        $count = 0;
        $string = null;
        $toText = (new Pdf($this->pdf_path))
        ->setPdf($this->fileHis)
        ->setOptions(['layout', 'r 96'])
        ->addOptions(['f 1'])
        ->text();
        $textToArr = explode("\n",$toText);
        
        foreach ($textToArr as $key => $value) {
            $string[$key] = str_replace('  ', '$', $value);
        }
        foreach ($string as $key => $value) {
            $explodeArr[$key] = explode("$",$value);
        }
        foreach ($explodeArr as $key => $value) {
            $removeEmp[$key] = array_values(array_filter($value));   
        }
        $removeEmpArr = array_values(array_filter($removeEmp));
        foreach ($removeEmpArr as $key => $value) {
            $removeSpace[$key] = preg_replace('/ /', '', $value, 1);
        }
    
        foreach ($removeSpace as $key => $value) { 
            if(count($value) == 1 && is_numeric($value[0]) !== true){
                    $removeArr[$key] = [];
            }else{
                if(count($value) == 1){
                    $count++;
                }
                $removeArr[$key] = $value;
            }
        }
            
        $removeArrEmp = array_values(array_filter($removeArr));
        $getAverage = count($removeArrEmp)/$count;
        $arrAverage = array_chunk($removeArrEmp,$getAverage);
        
        foreach($arrAverage as $key => $value){
            $head = $value[0];
            $getValue = array_slice($value, 1);
            
            array_unshift($getValue[0],'title');
            foreach ($getValue as $inkey => $invalue) {
        
               foreach ($invalue as $in2key => $in2value) {
                   if($inkey > 0){
                    $getHis[$key][$inkey][$getValue[0][$in2key]] = $getValue[$inkey][$in2key];
                   }else{
                    $getHis[$key][$inkey][$getValue[0][$in2key]] = $this->title;
                   }
                  
               }
          
            }

            $getHis[$key][0]=$head[0];        
        
        }
        return $getHis;
                
    }
}
