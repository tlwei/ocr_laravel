<?php
// Our Controller 
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
// This is important to add here. 
use PDFA;
  
class CustomerController extends Controller
{
    public function printPDF()
    {
       // This  $data array will be passed to our PDF blade

       $data = [
          'title' => 'Title',
          'heading' => 'Head',
          'content' => "Content..."   
        ];
        
        $pdf = PDFA::loadView('pdf_view', $data);  
        return $pdf->download('finance.pdf');
    }
}