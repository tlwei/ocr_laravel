<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Spatie\PdfToText\Pdf;
use App\financeReport;


class DocxConversion extends Controller
{
    //
    private $filename;

    
    public function convertToText() {

        if(isset($this->filename) && !file_exists($this->filename)) {
            return "File Not exists";
        }

        $file_ext = $this->filename->getClientOriginalExtension();

            if($file_ext == "doc") {
                return $this->read_doc();
            }elseif($file_ext == "docx") {
                return $this->read_docx();
            }elseif($file_ext == "xlsx") {
                return $this->xlsx_to_text($this->filename);
            }elseif($file_ext == "pptx") {
                return $this->pptx_to_text($this->filename);
            }elseif($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "png") {
                return $this->read_img($this->filename);
            }elseif($file_ext == "pdf") {
                return $this->read_pdf($this->filename);
            }else {
                return "Invalid File Type, accepted file type : doc/docx/pdf/xlsx/pptx/jpg/jpeg/png";
            }
 
    }

    public function __construct($filePath) {
        $this->filename = $filePath;
    }

    public function read_img($filename){
        return (new TesseractOCR($filename))->run();   
    }

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    public function read_pdf($filename){
        $server_file = $filename;
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($server_file);

        $striped_content = $pdf->getText();  
        // $text = nl2br($original_text); // Paragraphs and line break formatting
        $toArr = explode("toArr", $striped_content);

        $removeEmp = array_values(array_filter(array_map('trim', $toArr), 'strlen'));

        foreach ($removeEmp as $key => $value) {
            $arrToArr[] = explode("insideArr", $value);
            $title = $arrToArr[0];
        }

        $arrToArr = [];
        foreach ($removeEmp as $key => $value) {
            $arrToArr[] = explode("insideArr", $value);
            $title = $arrToArr[0];
        }
        
        //filter only data
        $check = true;
        $head = null;
        foreach ($arrToArr as $key => $value) {
            // if(count($value) == 1){
                // array_splice($arrToArr, $key, 1);
                // $arrToArr[$key] = [];
            // }
           if($check == true){
                $count = strpos($arrToArr[$key][0],'#');
                if($count !== false && $count == 0){
                    $head = $key;
                    $check = false;
                }
           }
        }

        if($head == null){
            return 'no header found';
        }

        //head > $arrToArr[$head]
        foreach ($arrToArr as $key => $value) {
            if($key<$head){
                $arrToArr[$key] = [];
            }
        }

        $filterArr = array_values(array_filter($arrToArr));
        
        //merge data
        foreach ($filterArr as $key => $value) {
            foreach ($value as $data_key => $data) {
                $finalArr[$key][$filterArr[0][$data_key]]=$filterArr[$key][$data_key];   
            }
        }
        
        // $finalArr[0] = ['Report title'=>$title[0]];
        financeReport::create([
            'title' => $title[0],
            'value' => json_encode($finalArr),
            'file_type' => 'pdf'
        ]);
        // return "Saved Report data to Database"."\n\n"."File Type: pdf"."\n\n".json_encode($finalArr);
        return $finalArr;
    }


    //////////
    //////////
    //////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    private function read_doc() {

        $fileHandle = fopen($this->filename, "r");
        $line = @fread($fileHandle, filesize($this->filename));   
        $lines = explode(chr(0x0D),$line);
        $outtext = "";
        foreach($lines as $thisline)
          {
            $pos = strpos($thisline, chr(0x00));
            if (($pos !== FALSE)||(strlen($thisline)==0))
              {
              } else {
                $outtext .= $thisline." ";
              }
          }
         $outtext = preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/"," ",$outtext);
        return $outtext;
    }

    private function read_docx(){
        
        $striped_content = '';
        $content = '';

        $zip = zip_open($this->filename);
        
        if (!$zip || is_numeric($zip)) return false;
        
        while ($zip_entry = zip_read($zip)) {
            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while
        
        zip_close($zip);
        //horizontal
        $content = str_replace('</w:r></w:p></w:tc><w:tc>', "insideArr", $content);
        //vertical
        $content = str_replace('</w:r></w:p>', "toArr", $content);

        $striped_content = strip_tags($content);
    
        $toArr = explode("toArr", $striped_content);
        //all array
        $removeEmp = array_values(array_filter(array_map('trim', $toArr), 'strlen'));

        $arrToArr = [];
        foreach ($removeEmp as $key => $value) {
            $arrToArr[] = explode("insideArr", $value);
            $title = $arrToArr[0];
        }
        
        //filter only data
        $check = true;
        $head = null;
        foreach ($arrToArr as $key => $value) {
            // if(count($value) == 1){
                // array_splice($arrToArr, $key, 1);
                // $arrToArr[$key] = [];
            // }
           if($check == true){
                $count = strpos($arrToArr[$key][0],'#');
                if($count !== false && $count == 0){
                    $head = $key;
                    $check = false;
                }
           }
        }
        if($head == null){
            return 'no header found';
        }
        //head > $arrToArr[$head]
        foreach ($arrToArr as $key => $value) {
            if($key < $head){
                $arrToArr[$key] = [];
            }
        }
        $filterArr = array_values(array_filter($arrToArr));
     
        //merge data
        foreach ($filterArr as $key => $value) {
            foreach ($value as $data_key => $data) {
                if($filterArr[$key][$data_key] !== ''){
                    $finalArr[$key][$filterArr[0][$data_key]]=$filterArr[$key][$data_key];   
                }
                
            }
        }

        $finalArrFilter = array_values(array_filter($finalArr));

        financeReport::create([
            'title' => $title[0],
            'value' => json_encode($finalArrFilter),
            'file_type' => 'docx'
        ]);

        // return "Saved Report data to Database"."\n\n"."File Type: docx"."\n\n".json_encode($finalArrFilter);
        return $finalArrFilter;
    }


    /************************excel sheet************************************/

    function xlsx_to_text($input_file){
        $xml_filename = "xl/sharedStrings.xml"; //content file name
        $zip_handle = new ZipArchive;
        $output_text = "";
        if(true === $zip_handle->open($input_file)){
            if(($xml_index = $zip_handle->locateName($xml_filename)) !== false){
                $xml_datas = $zip_handle->getFromIndex($xml_index);
                $xml_handle = DOMDocument::loadXML($xml_datas, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
                $output_text = strip_tags($xml_handle->saveXML());
            }else{
                $output_text .="";
            }
            $zip_handle->close();
        }else{
        $output_text .="";
        }
        return $output_text;
    }

    /*************************power point files*****************************/
    function pptx_to_text($input_file){
        $zip_handle = new ZipArchive;
        $output_text = "";
        if(true === $zip_handle->open($input_file)){
            $slide_number = 1; //loop through slide files
            while(($xml_index = $zip_handle->locateName("ppt/slides/slide".$slide_number.".xml")) !== false){
                $xml_datas = $zip_handle->getFromIndex($xml_index);
                $xml_handle = DOMDocument::loadXML($xml_datas, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
                $output_text .= strip_tags($xml_handle->saveXML());
                $slide_number++;
            }
            if($slide_number == 1){
                $output_text .="";
            }
            $zip_handle->close();
        }else{
        $output_text .="";
        }
        return $output_text;
    }

}
