<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class plResult extends Model
{
    //
    protected $table = 'pl_result';
    protected $fillable = [
        'title', 'value','file_type'
    ];
}
